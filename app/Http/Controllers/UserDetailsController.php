<?php

namespace App\Http\Controllers;

use App\Models\UserDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class UserDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userInfo = UserDetail::get();

        return view('TaskDetails.index', compact('userInfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Add User';
        return view('TaskDetails.create', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            // validation
            if ($request->category == 'image') {
                $validation = \Illuminate\Support\Facades\Validator::make($request->all(), [
                    'username' => 'required',
                    'phone' => 'required|numeric',
                    'category' => 'required',
                    'image' => 'mimes:jpeg,jpg,png,gif|required|max:10000'
                ]);
            } else {
                $validation = \Illuminate\Support\Facades\Validator::make($request->all(), [
                    'username' => 'required',
                    'phone' => 'required|numeric',
                    'category' => 'required',
                    'video' => 'mimes:mp4,mov,ogg,qt | max:20000'
                ]);
            }
            if ($validation->fails()) {
                Session::flash('error', __('messages.invalidvalidation'));
                Session::flash('alert-class', __('messages.alertdanger'));
                return redirect()->back();
            } else {

                if ($request->hasFile('image') && !empty($request->file('image'))) {
                    $image = $request->file('image');
                    $imagePath = public_path('gallery/images/');
                    File::isDirectory($imagePath) or File::makeDirectory($imagePath, 0777, true, true);
                    $fileOrgname = $image->getClientOriginalName();
                    $profilename = str_replace([' ', '-'], '_', rand(0, 10000) . '_' . time() . '_' . $fileOrgname);
                    $image->move($imagePath, $profilename);
                } else {
                    $video = $request->file('video');
                    $videoPath = public_path('gallery/videos/');
                    File::isDirectory($videoPath) or File::makeDirectory($videoPath, 0777, true, true);
                    $fileOrgname = $video->getClientOriginalName();
                    $profilename = str_replace([' ', '-'], '_', rand(0, 10000) . '_' . time() . '_' . $fileOrgname);
                    $video->move($videoPath, $profilename);

                }
                // user data store
                $userData = array(
                    'username' => $request->username,
                    'mobile' => $request->phone,
                    'gallery' => $request->category,
                    'image' => isset($request->image) ? $profilename : null,
                    'video' => isset($request->video) ? $profilename : null,
                );
                $userCreate = UserDetail::create($userData);
                Session::flash('success', __('messages.userCreated'));
                Session::flash('alert-class', __('messages.alertsuccuess'));
                return redirect()->route('dashboard');

            }
        } catch (\Exception $e) {
            Session::flash('error', __('messages.invalidcode'));
            Session::flash('alert-class', __('messages.alertdanger'));
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
