<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::middleware(['auth'])->group(function () {
    Route::group(['namespace' => 'User', 'prefix' => 'user'], function () {
        Route::get('userdetails', [\App\Http\Controllers\UserDetailsController::class, 'index'])->name('dashboard');
        Route::get('userdetails_create', [\App\Http\Controllers\UserDetailsController::class, 'create'])->name('dashboard.create');
        Route::post('userdetails_submit', [\App\Http\Controllers\UserDetailsController::class, 'store'])->name('dashboard.store');
    });
});

require __DIR__ . '/auth.php';
