<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="row">
                        <div class="col-lg-12 margin-tb">

                            <div class="pull-right">
                                <a class="btn btn-success" href="{{route('dashboard.create')}}"
                                   title="Create a product">
                                    Add New User
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>

                    <div class="content" style="padding: 1em;margin: 0 16px">
                        @if(\Illuminate\Support\Facades\Session::has('success'))
                            <p class="alert {{\Illuminate\Support\Facades\Session::get('alert-class', 'alert-info')}} alert-dismissible"
                               id="time">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{ \Illuminate\Support\Facades\Session::get('success') }}
                            </p>
                        @elseif(\Illuminate\Support\Facades\Session::has('error'))
                            <p class="alert {{\Illuminate\Support\Facades\Session::get('alert-class', 'alert-info')}} alert-dismissible"
                               id="time">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{ \Illuminate\Support\Facades\Session::get('error') }}
                            </p>
                        @endif
                    </div>
                    <table class="table table-bordered table-responsive-lg">
                        <tr>
                            <th>User Name</th>
                            <th>Mobile Number</th>
                            <th>Type</th>
                            <th>Gallery</th>
                        </tr>
                        @forelse ($userInfo as $product)
                            <tr>
                                <td>{{$product->username}}</td>
                                <td>{{$product->mobile}}</td>
                                <td>{{$product->gallery}}</td>
                                <td>
                                    @if(isset($product->image))
                                        <img src="{{URL::asset('/gallery/images/'.$product->image)}}"
                                             class="css-class" alt="alt text" height="100px" width="100px">
                                    @else
                                        <a onclick="show_my_receipt('{{$product->video}}')">click here</a>
                                    @endif
                                </td>

                            </tr>
                        @empty
                            <tr>
                                <td>Recoard Not Found</td>
                            </tr>
                        @endforelse
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
    <script>
        function show_my_receipt(videoName) {
            var url = '<?php echo \Illuminate\Support\Facades\URL::asset('/gallery/videos/') ?>';
            var fullUrl = url + '/' + videoName;
            var win = window.open();
            win.document.write('<iframe width="560" height="315" src="' + fullUrl + '" frameborder="0" allowfullscreen></iframe>')
        }
    </script>
</x-app-layout>
