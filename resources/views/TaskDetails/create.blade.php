<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{$title}}
        </h2>
    </x-slot>

    <div class="py-12 px-5">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 ">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg" style="padding: 20px;">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>{{$title}} </h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{route('dashboard')}}" title="Go back"> Back </a>
                        </div>
                    </div>
                </div>
                <div class="content" style="padding: 1em;margin: 0 16px">
                    @if(\Illuminate\Support\Facades\Session::has('success'))
                        <p class="alert {{\Illuminate\Support\Facades\Session::get('alert-class', 'alert-info')}} alert-dismissible"
                           id="time">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ \Illuminate\Support\Facades\Session::get('success') }}
                        </p>
                    @elseif(\Illuminate\Support\Facades\Session::has('error'))
                        <p class="alert {{\Illuminate\Support\Facades\Session::get('alert-class', 'alert-info')}} alert-dismissible"
                           id="time">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ \Illuminate\Support\Facades\Session::get('error') }}
                        </p>
                    @endif
                </div>
                <form
                    action="{{ route('dashboard.store')}}"
                    method="POST" id="userform" enctype='multipart/form-data'>
                    @csrf

                    <div class="form-group">
                        <label for="exampleInputEmail1">User Name</label>
                        <input type="text" name="username" class="form-control" id="price" aria-describedby="emailHelp"
                               placeholder="Enter UserName">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Mobile Number</label>
                        <input type="tel" id="phone" name="phone" class="form-control" placeholder="123-45-678">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Select Catagory</label>
                        <select class="browser-default custom-select form-control" name="category" id="category">
                            <option selected>category</option>
                            <option value="image">image</option>
                            <option value="video">video</option>
                        </select>
                    </div>
                    <div class="form-group" id="uploadFile">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script>
        if ($("#userform").length > 0) {
            $("#userform").validate({
                rules: {
                    username: {
                        required: true,
                    },
                    phone: {
                        required: true,
                        digits: true,
                        minlength: 10,
                        maxlength: 10,
                    },
                    category: {
                        required: true,

                    },
                },
                messages: {},
            })
        }
    </script>
    <script>
        $("#category").change(function () {
            $('#uploadFile').empty();
            var dropDownVal = $(this).val();
            if (dropDownVal == 'video') {
                $('#uploadFile').prepend($('<label>VideoUpload</label> <input type="file" class="form-control" id="image" name="'+dropDownVal+'" aria-describedby="emailHelp" value=""> <small id="emailHelp" class="form-text text-muted"></small>'));
            } else {
                $('#uploadFile').prepend($('<label>ImageUpload</label> <input type="file" class="form-control" id="video" name="'+dropDownVal+'" aria-describedby="emailHelp"value=""> <small id="emailHelp" class="form-text text-muted"></small>'));
            }
        });
    </script>
</x-app-layout>
